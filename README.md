### Komputer Store
By using HTML, Javascript and CSS in Visual Studio Code, I have created a single page application that includes a bank, work, and laptop features. The program allows you to work to make money that you can bank. In the bank you can take up a loan that you can pay down. You can buy a variety of laptops that are fetched from an API. The laptops name, description, features, image and most importantly: the price is displayed for each laptop. If you have enough money in the bank, you can buy your own virtual laptop!

### How to run the program
There are two requirements to run the program:

1. An IDE to run the program, eligibly VS Code.
2. Live Server VS Code Extension

After cloning the repository, open the index.html file and open with the Live Server extension. Now you can play around with all the cool features the application has!

### Contributors:
Håvard Madland: @havardmad