let balance = 0;
let loan = 0;
let salary = 0;
let price = 0;
let features = [];
let laptops = [];

// Moneydisplay HTML elements.
let balanceText = document.getElementById("balance-amount");
let loanText = document.getElementById("loan-amount");
let salaryText = document.getElementById("pay-amount");

// Laptop HTML elements.
const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const laptopHeaderElement = document.getElementById("laptop-header");
const laptopDescriptionElement = document.getElementById("laptop-description");
const laptopPriceElement = document.getElementById("price-tag");
const laptopImageElement = document.getElementById("image");

// Button HTML elements.
const loanBtn = document.getElementById("loan-btn");
const bankBtn = document.getElementById("bank-btn");
const workBtn = document.getElementById("work-btn");
const repayLoanBtn = document.getElementById("repay-loan-btn");
const buyBtn = document.getElementById("buy-btn");

// Hides loan elements on initial load.
document.getElementById("loan").style.visibility = "hidden";
document.getElementById("repay-loan-btn").style.visibility = "hidden";

// Fetches API data and runs addLaptopsToList function.
fetch("https://hickory-quilled-actress.glitch.me/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToList(laptops));

// Calls addLaptopToList function and creates all initial display of the first fetched pc.
const addLaptopsToList = (laptops) => {
  laptops.forEach((x) => addLaptopToList(x));
  featuresElement.innerText = laptops[0].specs;
  laptopHeaderElement.innerText = laptops[0].title;
  laptopDescriptionElement.innerText = laptops[0].description;
  laptopPriceElement.innerText = applyNOK(laptops[0].price);
  price = laptops[0].price;
};

// Adds all laptops to drop down menu.
const addLaptopToList = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};

// When another laptop is selected in the drop down menu all laptop display elements changes.
const handleLaptopChange = (e) => {
  var selectedLaptop = laptops[e.target.selectedIndex];
  featuresElement.innerText = selectedLaptop.specs;
  laptopHeaderElement.innerText = selectedLaptop.title;
  laptopDescriptionElement.innerText = selectedLaptop.description;
  laptopPriceElement.innerText = applyNOK(selectedLaptop.price);
  price = selectedLaptop.price;
  let imageAPI = "https://hickory-quilled-actress.glitch.me/";
  imageAPI += selectedLaptop.image;
  laptopImageElement.src = imageAPI;
};

/*
Creates a loan. Checks if another loan is already issued and if loaned amount is less than double of bank balance.
Then creates a loan and adds loaned amount to balance and loan elements are displayed to the user.
*/
function getLoan() {
  if (loan === 0) {
    window.alert(
      "Note: You can only have one loan at a time and you cannot get a loan more than double of your bank balance."
    );
    const loanAmount = parseInt(
      window.prompt("Insert amount you want to loan")
    );
    let maxLoan = balance * 2;

    if (loanAmount <= maxLoan) {
      balance += loanAmount;
      loan = loanAmount;
      balanceText.innerText = applyNOK(balance);
      loanText.innerText = applyNOK(loan);
      document.getElementById("loan").style.visibility = "visible";
      document.getElementById("repay-loan-btn").style.visibility = "visible";
    } else window.alert("The max loan you can take is: " + maxLoan);
  } else window.alert("You can only have one loan at a time");
}

/*
Banks the money in work. First checks if there is money in work, then checks if user has a loan
and if 10% of work money is more than loan. Then does the calculations and adds result to HTML elements.
*/
function bankMoney() {
  if (salary > 0) {
    if (loan > 0) {
      if (salary * 0.1 >= loan) {
        let remainingSalary = salary - loan;
        balance += remainingSalary;
        salary = 0;
        loan = 0;
        fullyPaidLoan();
      } else {
        loan -= salary * 0.1;
        balance += salary * 0.9;
        salary = 0;
      }
    } else {
      balance += salary;
      salary = 0;
    }
  } else window.alert("Unable to bank money. You have to work first");

  balanceText.innerText = applyNOK(balance);
  loanText.innerText = applyNOK(loan);
  salaryText.innerText = applyNOK(salary);
}

// Work to get money.
function work() {
  salary += 100;
  salaryText.innerText = applyNOK(salary);
}

/*
Repays money in work to issued loan. First checks if salary is higher than loan and prints message to user that loan is fully paid.
If not, subtracks loan by sum in work and adds result to HTML elements.
*/
function repayLoan() {
  if (salary > loan) {
    let remainingSalary = salary - loan;
    balance += remainingSalary;
    salary = 0;
    loan = 0;
    if (loan === 0) {
      fullyPaidLoan();
    }
  } else {
    loan -= salary;
    salary = 0;
    if (loan === 0) {
      fullyPaidLoan();
    }
  }
  balanceText.innerText = applyNOK(balance);
  salaryText.innerText = applyNOK(salary);
  loanText.innerText = applyNOK(loan);
}

// Buys a laptop. Checks if enough money in bank.
function buyLaptop() {
  if (balance >= price) {
    balance -= price;
    balanceText.innerText = applyNOK(balance);
    window.alert("You are now the owner of the new laptop!");
  } else window.alert("You don't have enough money to buy this laptop..");
}

// Helperfunction to apply NOK format to money display.
function applyNOK(number) {
  return new Intl.NumberFormat("no-NO", {
    style: "currency",
    currency: "NOK",
  }).format(number);
}

// Helperfunction to hide loan elements when loan is fully paid.
function fullyPaidLoan() {
  return (
    window.alert("Your loan has been fully paid!") +
    (document.getElementById("repay-loan-btn").style.visibility = "hidden") +
    (document.getElementById("loan").style.visibility = "hidden")
  );
}

// EventListeners
loanBtn.addEventListener("click", getLoan);
bankBtn.addEventListener("click", bankMoney);
workBtn.addEventListener("click", work);
repayLoanBtn.addEventListener("click", repayLoan);
laptopsElement.addEventListener("change", handleLaptopChange);
buyBtn.addEventListener("click", buyLaptop);